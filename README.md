# getQuestionOrder

Use questions to get the ordered lits of randomized question and group

## Installation

### Via GIT

- Go to your LimeSurvey Directory
- Clone in plugins/questionExtraSurvey directory `git clone https://gitlab.com/SondagesPro/QuestionSettingsType/getQuestionOrder.git getQuestionOrder`

### Via ZIP dowload

- Download <https://dl.sondages.pro/getQuestionOrder.zip>
- Extract : `unzip getQuestionOrder.zip`
- Move the directory to  plugins/ directory inside LimeSUrvey

## Usage

Look at default question title used by this plugin in plugin setting : by default `getOrderQ` and `getOrderG`. You can set the separator used for question and for group.

If you create question with this code : it was used to put the question order. **No other control is done on question** then best is to use a text question type, and hide it using advanced settings.

You can put this question at start of the survey or anywhere. 

- **getOrderQ** get all the question code in final order show to participant.
- **getOrderG** get the id of randomized group in final order show to participant.
